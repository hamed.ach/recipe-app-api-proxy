# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Host to connect to (default: 'app')
* 'APP_PORT' - Port to connect to (default: '9000') 